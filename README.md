# Guida per l'installazione di Arch Linux (aggiornata al 27/02/2022)

Questa è una guida passo passo per l'installazione di base di Arch Linux.

Nella prima sezione della guida vengono elencati i principali comandi per l'installazione e configurazione di base di Arch Linux, nelle sezioni successive come installare desktop grafici come Cinnamon o Xfce, oltre che vari programmi, servizi, risoluzione di alcuni problemi e varie.

## Sommario

* [Informazioni generali](#informazioni-generali)
* [**Prima parte - Installazione base di Arch**](#prima-parte)
  - [1) Caricare il layout di tastiera](#caricare-layout-tastiera)
  - [2) Verificare se la rete è attiva](#verificare-rete-attiva)
  - [3) Verificare se la rete è connessa](#verificare-rete-connessa)
  - [4) Abilitare il server ntp](#abilitare-ntp)
  - [5) Installare il sistema in modalità UEFI](#installare-modalita-uefi)
  - [6) Verificare in quale disco installare il sistema](#disco-installazione)
  - [7) Partizionare il disco con fdisk o cfdisk](#partizionare-disco)
  - [8) Creare i file system nel disco di installazione](#creare-filesystem)
  - [9) Montare le partizioni](#montare-partizioni)
  - [10) Installare i pacchetti base](#installare-pacchetti-base)
  - [11) Generare il file fstab per la tabella delle partizioni](#generare-fstab)
  - [12) Eseguire il comando arch-chroot](#arch-chroot)
  - [13) Creare lo Swapfile in alternativa alla partizione swap](#swapfile)
  - [14) Settare il time zone](#timezone)
  - [15) Localizzazione](#localizzazione)
  - [16) Rendere persistente la modifica del layout di tastiera](#layout-tastiera)
  - [17) Impostazione dell'host name](#hostname)
  - [18) Modificare il file hosts](#file-hosts)
  - [19) Impostare la password di root](#password-root)
  - [20) Installare pacchetti fondamentali per il sistema](#pacchetti-fondamentali)
  - [21) Installare grub nel disco di installazione](#installare-grub)
  - [22) Abilitare il NetworkManager](#abilitare-networkManager)
  - [23) Aggiungere un utente non-root](#aggiungere-utente)
  - [24) Finalizzazione](#finalizzazione)
* [**Seconda parte - Post installazione**](#seconda-parte)
  - [1) Verificare se la rete è attiva](#post-verificare-rete-attiva)
  - [2) Abilitare SSH](#post-abilitare-ssh)
  - [3) Ottimizzare i repository con reflector](#post-reflector)
  - [4) Installare YAY o PARU helper da terminale](#post-aur-helper)
  - [5) Installare server e driver video](#post-server-video)
  - [6) Installare fonts utili](#post-installare-fonts-utili)
  - [7) Installare e abilitare il login grafico LightDM](#post-installare-lightdm)
  - [8) Configurare LightDM](#post-configurare-lightdm)
  - [9) Problemi di avvio di LightDM](#problemi-avvio-lightdm)
  - [10) Slim, alternativa a LightDM](#post-installare-slim)
  - [11) Installare PAMAC](#post-installare-pamac)
  - [12) Installare pacchetti utili](#post-installare-pacchetti-utili)
  - [13) Uniformare lo stile delle applicazioni QT5 con stile GTK+](#post-stile-qt5)
  - [14) Installare SAMBA](#post-installare-samba)
* [**Terza parte - Installazione di un desktop grafico**](#terza-parte)
  - [Installazione del desktop grafico Cinnamon](#desktop-cinnamon)
  - [Installazione del desktop grafico Xfce](#desktop-xfce)
* [**Quarta parte - Installazione di software vario**](#quarta-parte)
  - [Stampa e drivers stampanti](#stampa)
  - [Fonts aggiuntivi](#fonts-aggiuntivi)
  - [Gestori archivi](#gestori-archivi)
  - [Browser Web e Client Email](#browser-mail)
  - [Codec e utility multimediali](#codec-multimedia)
  - [Oracle JAVA e OpenJDK](#java)
  - [Temi GTK (opzionali)](#temi-gtk)
  - [Temi icone (opzionali)](#temi-icone)
  - [Wallpapers (opzionali)](#wallpapers)
  - [Programmi di grafica (opzionali)](#grafica)
  - [Virtualbox](#virtualbox)
  - [Virt-Manager, Qemu e Libvirtd (alternativa a Virtualbox)](#virt-manager)
  - [Gestori Downloads](#gestori-downloads)
  - [Skype (opzionale)](#skype)
  - [Libreoffice](#libreoffice)
  - [Owncloud client](#owncloud-client)
  - [Visual Studio Code](#code)
  - [Clients Chat](#clients-chat)
  - [Wine (opzionale)](#wine)
  - [Bleachbit (opzionale)](#bleachbit)
  - [Remmina (opzionale)](#remmina)
  - [ocs-url (AUR)](#ocs-url)
  - [key-mon (AUR)](#key-mon)
  - [ulauncher (AUR)](#ulauncher)
  - [update-grub (AUR)](#update-grub)
  - [hblock (AUR)](#hblock)
  - [hardcode-tray-git (AUR)](#hardcode-tray)
  - [timeshift (AUR)](#timeshift)
  - [teamviewer (AUR)](#teamviewer)
  - [slurm (AUR)](#slurm)
  - [pfetch (AUR)](#pfetch)
  - [qownnotes (AUR)](#qownnotes)
  - [clipit (AUR)](#clipit)
  - [toilet (AUR)](#toilet)
  - [conky-lua (AUR)](#conky-lua)
  - [gnome-system-tools (AUR)](#gnome-system-tools)
  - [Utility varie](#utility-varie)
  - [Altri pacchetti utili](#altri-pacchetti-utili)
* [**Quinta parte - Alcune ottimizzazioni di sistema**](#quinta-parte)
  - [Aggiornare le chiavi di autenticazione](#chiavi-autenticazione)
  - [Crontab (cronie)](#cronie)
  - [Abilitare il servizio per la pulizia cache pacchetti](#pulizia-cache-pacchetti)
  - [Solid State Drive (SSD trim)](#ssd-trim)
  - [Problemi relativi all'hardware audio](#hw-audio)
* [**Sesta parte - risorse varie dal web**](#sesta-parte)
  - [Arch Linux: Full Installation Guide - A complete tutorial/walkthrough in one video!](#risorsa1)
  - [Connect Android To Arch Linux Via USB – Connect over MTP in Linux](#risorsa2)
  - [How to clean Arch Linux](#risorsa3)
  - [ArcoLinux Listing - Elenco di utili risorse valide anche per Arch Linux](#risorsa4)
  - [How To Speed Up Compilation Process When Installing Packages From AUR](#risorsa5)
  - [Enable High Quality Audio on Linux](#risorsa6)
  - [Different Types of Kernel for Arch Linux and How to Use Them](#risorsa7)
  - [vinceliuice on Github - grub2 themes](#risorsa8)
  - [Glava - An embed audio visualizer on your linux](#risorsa9)
  - [MacOS like Fonts on Manjaro/Arch Linux](#risorsa10)

<a name="informazioni-generali"></a>
## Informazioni generali

Il file ISO per l'installazione di base di Arch Linux può essere scaricato dalla seguente pagina:

**https://archlinux.org/download/**

successivamente, il file iso può essere scritto in una chiavetta usb tramite Balena Etcher ( **https://www.balena.io/etcher/** ) che è il programma più comune e facile da usare per trasferire file ISO e rendere avviabile qualsiasi chiavetta USB, oppure da terminale tramite il comando:

`sudo dd bs=4M if=path/to/archlinux.iso of=/dev/sdx status=progress oflag=sync`

leggi anche la pagina ufficiale del wiki: **https://wiki.archlinux.org/index.php/USB_flash_installation_medium**

Alternativamente, si può utilizzare anche **usbimager** che supporta diverse piattaforme tra cui Windows, MacOSX, Ubuntu LTS, Arch/Manjaro, Raspberry Pi, vedi:

**https://gitlab.com/bztsrc/usbimager**

La prima parte, **Installazione base di Arch** dove viene spiegato passo passo come installare Arch Linux base, può essere saltata se viene utilizzata l'installazione grafica **Calam Installer**, la quale semplica notevolmente l'installazione di base del sistema operativo. Si può scaricare la iso di Calam Installer da questa pagina:

**https://sourceforge.net/projects/blue-arch-installer/files/arch-installer/**

Se si installa Arch Linux in Virtualbox, creare inizialmente una macchina virtuale; per l'installazione di Virtualbox e la creazione di una macchina virtuale Arch si rimanda ai vari tutorial che si possono trovare su Youtube o su Internet.

<a name="prima-parte"></a>
## Prima parte - Installazione base di Arch

Per il tutorial ufficiale di Arch Linux sull'installazione vedi:

**https://wiki.archlinux.org/index.php/Installation_guide**

Per cominciare, avviare Arch dalla chiavetta precedentemente creata, al prompt eseguire i comandi elencati.

1) <a name="caricare-layout-tastiera"></a> **Caricare il layout di tastiera**

	`loadkeys it`
	
	**Opzionale**: per cambiare il tipo font:
	
	`setfont ter-132n`

2) <a name="verificare-rete-attiva"></a> **Verificare se la rete è attiva**

	`ip a`
	
	oppure:
	
	`ip link`
	
	Per connettere la WiFi ad una rete wireless utilizzare il comando iwctl ( **https://wiki.archlinux.org/index.php/Iwd#iwctl** )

3) <a name="verificare-rete-connessa"></a> **Verificare se la rete è connessa**

	`ping www.google.com`
	
	oppure:
	
	`ping -c 5 8.8.8.8`

4) <a name="abilitare-ntp"></a> **Abilitare il server ntp**

	`timedatectl set-ntp true`

5) <a name="installare-modalita-uefi"></a> **Installare il sistema in modalità UEFI**

	Per installare il sistema in modalità UEFI, verificare se si è avviato il live system in modalità UEFI con il seguente comando:

	`ls /sys/firmware/efi/efivars`

	se la directory non è vuota il sistema è in modalità UEFI.

6) <a name="disco-installazione"></a> **Verificare in quale disco installare il sistema**

	`lsblk`

	oppure:

	`fdisk – l | less`

7) <a name="partizionare-disco"></a> **Partizionare il disco con fdisk o cfdisk**

	Il disco dove verrà installato Arch può essere partizionato e formattato secondo due modalità diverse, con il comando fdisk tradizionale, oppure tramite il più comodo cfdisk.

	**Partizionamento con fdisk**

	`fdisk /dev/sdX [INVIO]`

	| comando       | funzione comando                       |
	| :-----------: | -------------------------------------- |
	| **a [INVIO]** | attiva / disattiva il flag avviabile   |
	| **g [INVIO]** | crea una tabella di partizioni GPT     |
	| **o [INVIO]** | crea una tabella di partizioni DOS     |
	| **p [INVIO]** | stampa la tabella delle partizioni     |
	| **n [INVIO]** | crea una nuova partizione              |
	| **t [INVIO]** | cambia il tipo di partizione           |
	| **w [INVIO]** | scrive le modifiche                    |

	**Partizionamento con cfdisk**

	`cfdisk [INVIO]`
	
	e utilizzare l’interfaccia grafica.

	Per esempio, installando su un disco da 240 Gb che viene riconosciuto come device /dev/sda, può essere suddiviso come segue:

	**modalità UEFI**

	| Device        | Spazio dedicato  | Tipo di file system          | Directory assegnata alla partizione |
	| :-----------: | ---------------- | ---------------------------- | ----------------------------------- |
	| **/dev/sda1**	| 550M             | EFI System                   | /boot/efi                           |
	| **/dev/sda2** | 2GB              | Linux Swap                   | -                                   |
	| **/dev/sda3** | 100GB            | Linux FileSystem             | /                                   |
	| **/dev/sda4** | spazio rimanente | Linux FileSystem             | /home                               |

	**modalità BIOS LEGACY**

	| Device        | Spazio dedicato  | Tipo di file system          | Directory assegnata alla partizione |
	| :-----------: | ---------------- | ---------------------------- | ----------------------------------- |
	| **/dev/sda1**	| 1GB              | boot                         | -                                   |
	| **/dev/sda2** | 2GB              | Linux Swap                   | -                                   |
	| **/dev/sda3** | 100GB            | Linux FileSystem             | /                                   |
	| **/dev/sda4** | spazio rimanente | Linux FileSystem             | /home                               |

	**NOTA BENE**: se si vuole attivare lo swapfile invece della partizione di swap, non tenere conto della partizione ‘dev/sda2’ (attenzione al cambio della numerazione delle partizioni nelle fasi successive, si avrà ‘/dev/sda1’ per la partizione di boot, ‘/dev/sda2’ per la partizione di root e ‘/dev/sda3’ per la partizione home).
	Vedi il **punto 13. Creare lo Swapfile in alternativa alla partizione swap**.

8) <a name="creare-filesystem"></a> **Creare i file system nel disco di installazione**

	**modalità UEFI**

	`mkfs.fat -F32 /dev/sda1` (boot)

	`mkswap /dev/sda2` (swap - solo se si utilizza lo swap come partizione)

	`swapon /dev/sda2`

	`mkfs.ext4 /dev/sda3` (root)

	`mkfs.ext4 /dev/sda4` (home)

	**modalità BIOS LEGACY**

	`mkfs.ext4 /dev/sda1` (boot)

	`mkswap /dev/sda2` (swap - solo se si utilizza lo swap come partizione)

	`swapon /dev/sda2`

	`mkfs.ext4 /dev/sda3` (root)

	`mkfs.ext4 /dev/sda4` (home)

9) <a name="montare-partizioni"></a> **Montare le partizioni**

	**modalità UEFI**

	`mount /dev/sda3 /mnt` (montaggio della partizione root in mnt)

	`mkdir -p /mnt/boot/EFI`

	`mkdir -p /mnt/home`

	`mount /dev/sda1 /mnt/boot/EFI` (montaggio della partizione EFI per il boot)

	`mount /dev/sda4 /mnt/home` (montaggio della partizione home)

	**modalità BIOS LEGACY**

	`mount /dev/sda3 /mnt` (montaggio della partizione root in mnt)

	`mkdir -p /mnt/boot`

	`mkdir -p /mnt/home`

	`mount /dev/sda1 /mnt/boot` (montaggio della partizione per il boot)

	`mount /dev/sda4 /mnt/home` (montaggio della partizione home)

	verificare che le partizioni siano montate correttamente con:

	`lsblk`

10) <a name="installare-pacchetti-base"></a> **Installare i pacchetti base**

	**base**, **linux** (oppure kernel **linux-lts**, **linux-zen**, **linux-hardened**), **linux-firmware**, **nano** (editor), **sudo**

	per un sistema con kernel **linux**:
	
	`pacstrap /mnt base linux linux-headers linux-docs linux-firmware nano sudo`

	per un sistema con kernel **linux-lts**:
	
	`pacstrap /mnt base linux-lts linux-lts-headers linux-lts-docs linux-firmware nano sudo`
	
	per un sistema con kernel **linux-zen**:
	
	`pacstrap /mnt base linux-zen linux-zen-headers linux-zen-docs linux-firmware nano sudo`
	
	per un sistema con kernel **linux-hardened**:
	
	`pacstrap /mnt base linux-hardened linux-hardened-headers linux-hardened-docs linux-firmware nano sudo`

	**NOTA BENE**: la voce **linux** riguarda il tipo di kernel; si può scegliere tra quattro tipi diversi di kernel:
	
	- **linux** (ultima versione di kernel linux stabile)
	
	- **linux-lts** (versione stabile con supporto a lungo termine)
	
	- **linux-zen** (versione ottimizzata da un gruppo di hackers per garantire migliori prestazioni nell’utilizzo di tutti i giorni)
	
	- **linux-hardened** (utilizzato per ambienti più sicuri).

11) <a name="generare-fstab"></a> **Generare il file fstab per la tabella delle partizioni**

	`genfstab -U /mnt >> /mnt/etc/fstab`

	per verificare la corretta generazione:

	`cat /mnt/etc/fstab`

12) <a name="arch-chroot"></a> **Eseguire il comando arch-chroot**

	`arch-chroot /mnt`

13) <a name="swapfile"></a> **Creare lo Swapfile in alternativa alla partizione swap**

	**NOTA BENE**: se non si utilizza lo swap come partizione, a questo punto generare e attivare uno swapfile con i seguenti comandi:

	`fallocate -l 2GB /swapfile`

	`chmod 600 /swapfile`

	`mkswap /swapfile`

	`swapon /swapfile`

	aggiungere lo swapfile al file fstab:

	`nano /etc/fstab`

	```
	/swapfile none swap defaults 0 0
	```

14) <a name="timezone"></a> **Settare il time zone**

	Per verificare quale timezone impostare, utilizzare il seguente comando:
	
	`ls -l /usr/share/zoneinfo/Europe/`
	
	e scorrere la lista fino ad individuare il timezone corretto. Nell'esempio seguente, per impostare il timezone **Europe/San_Marino** o **Europe/Rome** per l'Italia:
	
	`ln -sf /usr/share/zoneinfo/Europe/San_Marino /etc/localtime`

	`hwclock --systohc`

15) <a name="localizzazione"></a> **Localizzazione**

	`nano /etc/locale.gen`

	decommentare:

	```
	#en_US.UTF-8 UTF8
	```

	poi eseguire il comando:

	`locale-gen`

	editare il file:

	`nano /etc/locale.conf`

	aggiungere la riga:

	```
	LANG=en_US.UTF-8
	```

16) <a name="layout-tastiera"></a> **Rendere persistente la modifica del layout di tastiera**

	`nano /etc/vconsole.conf`

	aggiungere la riga:

	```
	KEYMAP=it
	```

17) <a name="hostname"></a> **Impostazione dell'host name**

	`hostnamectl set-hostname [NOME_HOST]`

	esempio:

	`hostnamectl set-hostname archvbox`

	verifica il corretto hostname:

	`hostnamectl [INVIO]`

	verificare se nel file /etc/hostname è memorizzato il nome host, altrimenti modificarlo con:

	`nano /etc/hostname`

18) <a name="file-hosts"></a> **Modificare il file hosts**

	`nano /etc/hosts`

	aggiungere le seguenti righe:

	```
	127.0.0.1   localhost
	::1         localhost
	127.0.1.1   archvbox.localdomain  archvbox
	```

	sostituire il nome host **archvbox** (di esempio) con quello corretto.

19) <a name="password-root"></a> **Impostare la password di root**

	`passwd root`

20) <a name="pacchetti-fondamentali"></a> **Installare pacchetti fondamentali per il sistema**

	**grub**, **efibootmgr**, **networkmanager**, **networkmanager-pptp**, **networkmanager-openvpn**, **network-manager-applet**, **dialog**, **os-prober**, **mtools**, **dosfstools**, **base-devel**, **git**, **pacman-contrib**

	`pacman -S grub efibootmgr networkmanager networkmanager-pptp networkmanager-openvpn network-manager-applet dialog os-prober mtools dosfstools base-devel git pacman-contrib`

21) <a name="installare-grub"></a> **Installare grub nel disco di installazione**

	**Modalità UEFI**

	`grub-install --target=x86_64-efi --efi-directory=/boot/EFI --bootloader-id=GRUB`

	**Modalità BIOS LEGACY**

	`grub-install /dev/sda`

	poi:

	`grub-mkconfig -o /boot/grub/grub.cfg`

22) <a name="abilitare-networkmanager"></a> **Abilitare il NetworkManager**

	`systemctl enable NetworkManager`

23) <a name="aggiungere-utente"></a> **Aggiungere un utente non-root**

	**[NOME_UTENTE]** = sostituire con un nome utente

	`useradd -m [NOME_UTENTE]`

	`passwd [NOME_UTENTE]`

	`usermod -aG wheel,audio,video,optical,storage [NOME_UTENTE]`

	esempio, per un nome utente giagio:

	`useradd -m giagio`

	`passwd giagio`

	`usermod -aG wheel,audio,video,optical,storage giagio`

	poi, eseguire il comando:

	`EDITOR=nano visudo`

	decommentare la riga:

	```
	#%wheel ALL=(ALL) ALL
	```

24) <a name="finalizzazione"></a> **Finalizzazione**

	Uscire dalla shell di chroot, smontare tutte le partizioni e riavviare:
	
	`exit`

	`umount -a; reboot`

<a name="seconda-parte"></a>
## Seconda parte - Post installazione

1) <a name="post-verificare-rete-attiva"></a> **Verificare se la rete è attiva**

	`ip a`

2) <a name="post-abilitare-ssh"></a> **Abilitare SSH**

	`sudo pacman -S openssh`

	`sudo systemctl start sshd`

	`sudo systemctl enable sshd`

3) <a name="post-reflector"></a> **Ottimizzare i repository con reflector**

	`sudo pacman -Syy reflector`

	`sudo reflector -c Italy -a 6 --sort rate --save /etc/pacman.d/mirrorlist`

	`sudo pacman -Syyy`

4) <a name="post-aur-helper"></a> **Installare YAY o PARU helper da terminale**

	**YAY (deprecato - fine sviluppo)**

	`mkdir ~/temp`

	`cd ~/temp`

	`git clone https://aur.archlinux.org/yay-git.git`

	`cd yay-git`

	`makepkg -sri`

	**PARU (vedi anche: https://github.com/Morganamilo/paru)**

	`mkdir ~/temp`

	`cd ~/temp`

	`git clone https://aur.archlinux.org/paru.git`

	`cd paru`

	`makepkg -si`

5) <a name="post-server-video"></a> **Installare server e driver video**

	Per il funzionamento del desktop grafico è necessario installare il server video xorg e i relativi driver open source o proprietari in base alla scheda video installata nel computer (vedi anche **https://wiki.archlinux.org/index.php/Xorg**). I pacchetti da installare sono:
	
	**xorg**

	**xf86-video-vmware** (drivers open source per Virtualbox / Vmware)

	**xf86-video-ati** (drivers open source per schede video ATI)

	**xf86-video-amdgpu** (drivers open source per schede video AMD)

	**xf86-video-nouveau** (drivers open source per schede NVIDIA)

	**nvidia**, **nvidia-settings**, **nvidia-utils** (drivers proprietari per schede video NVIDIA con kernel **linux**)

	**nvidia-lts**, **nvidia-settings**, **nvidia-utils** (drivers proprietari per schede video NVIDIA con kernel **linux-lts**)

	**NOTA BENE**: in alternativa ai drivers open source xf86-video-vmware puoi usare i drivers proprietari di Virtualbox, **virtualbox-guest-utils** oppure i drivers proprietari **open-vm-tools** per macchina virtuale VmWare).

	**Macchina virtuale Virtualbox o VmWare - drivers open source**

	`sudo pacman -S xorg xf86-video-vmware`

	**Macchina virtuale Virtualbox - Guest Additions drivers proprietari**

	`sudo pacman -S xorg virtualbox-guest-utils` (Virtualbox Guest Additions - drivers proprietari)

	`sudo systemctl enable vboxservice.service`

	**Macchina virtuale VmWare - drivers proprietari**

	`sudo pacman -S xorg open-vm-tools net-tools gtkmm` (macchina virtuale VmWare - drivers proprietari)

	**Drivers open source schede video ATI**

	`sudo pacman -S xorg xf86-video-ati`

	**Drivers open source schede video AMD**

	`sudo pacman -S xorg xf86-video-amdgpu`

	**Drivers open source schede video NVIDIA**

	`sudo pacman -S xorg xf86-video-nouveau`

	**Drivers proprietari schede video NVIDIA**

	`sudo pacman -S xorg nvidia nvidia-utils`
	
	**NOTA BENE:** i drivers proprietari Nvidia, per esperienza personale, possono interferire con il corretto funzionamento del login manager che sia lightdm, slim o altri. Preferire i drivers open source.
	
	**Fix del 04/10/2021**: per risolvere il problema dei drivers proprietari di nvidia, mettere in blacklist il driver i915 modificando il file /etc/modprobe.d/modprobe.conf, inserendo le linee:

	blacklist i915
	
	install i915 /usr/bin/false
	
	poi rimuovere eventuali driver open source:
	
	`sudo pacman -R xf86-video-nouveau xf86-video-vesa`

	poi installare:
	
	`sudo pacman -S nvidia nvidia-lts nvidia-utils nvidia-settings opencl-nvidia`
	
	poi:
	
	`sudo nano /etc/mkinitcpio.conf`
	
	e inserire nella sezione MODULES:
	
	MODULES="nvidia nvidia_modeset nvidia_uvm nvidia_drm"
	
	infine:
	
	`sudo mkinitcpio -P linux`

6) <a name="post-installare-fonts-utili"></a> **Installare fonts utili**

	`sudo pacman -S gsfonts sdl_ttf xorg-fonts-type1`
	
	`paru font-bh-ttf`

7) <a name="post-installare-lightdm"></a> **Installare e abilitare il login grafico LightDM**

	I pacchetti necessari per far funzionare il login grafico lightdm sono i seguenti:

	**lightdm**, **lightdm-gtk-greeter**, **lightdm-gtk-greeter-settings**

	Per installarli digitare il seguente comando:

	`sudo pacman -S lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings`

	per abilitare il login grafico all'avvio del sistema:

	`sudo systemctl enable lightdm.service`

8) <a name="post-configurare-lightdm"></a> **Configurare LightDM**

	Vedi anche: **https://wiki.archlinux.org/index.php/LightDM**

	Per il login grafico lightdm si ha a disposizione una ampia scelta di greeter. Qui di seguito sono elencati quelli che possono essere installati:

	**GTK greeter**

	`sudo pacman -S lightdm-gtk-greeter`

	**Webkit2 greeter**

	`sudo pacman -S lightdm-webkit2-greeter`

	`paru lightdm-webkit-theme-aether` (tema per webkit2 greeter)

	**Slick greeter (come login grafico di Linux Mint)**

	`paru lightdm-slick-greeter`

	**Mini greeter**

	`paru lightdm-mini-greeter`

	Abilitare il greeter nel file lightdm.conf:

	`sudo nano /etc/lightdm/lightdm.conf`

	cercare la stringa:

	```
	#greeter-session=example-gtk-gnome
	```

	rimuovere il simbolo #, e sostituire example-gtk-gnome con il nome di una delle sessioni seguenti: lightdm-gtk-greeter, lightdm-webkit2-greeter, lightdm-slick-greeter, lightdm-mini-greeter.

	Dopo aver salvato le modifiche, testa i settaggi di LightDM e conferma che lavorano con dm-tool:

	`lightdm --test-mode –debug`

	**LightDM come display manager primario**

	**Passo 1**: disabilita il login manager corrente con systemd disable:

	`sudo systemctl disable gdm -f`

	o:

	`sudo systemctl disable sddm -f`

	o:

	`sudo systemctl disable lxdm -f`

	**Passo 2**: abilita LightDM con systemctl enable:

	`sudo systemctl enable lightdm -f`

	**Passo 3**: riavvia il tuo PC Arch Linux usando il comando **systemctl reboot**. Assicurati di aver salvato qualsiasi lavoro tu stia facendo prima di avviare tale comando!

	**Passo 4**: Lascia che Arch Linux si riavvii. Al termine dell'avvio, vedrai LightDM apparire come la nuova schermata di accesso.

	Fonte: **https://techniorg.com/how-to-set-up-lightdm-on-arch-linux-guide/**
	
9) <a name="problemi-avvio-lightdm"></a> **Problemi di avvio di LightDM**

	Links per risolvere eventuali problemi di avvio di lightdm:

	**https://wiki.archlinux.org/index.php/LightDM#LightDM_does_not_appear_or_monitor_only_displays_TTY_output**
	**https://wiki.archlinux.org/index.php/Kernel_mode_setting#Early_KMS_start**

10) <a name="post-installare-slim"></a> **Slim, alternativa a LightDM**

	`sudo pacman -S slim slim-themes archlinux-themes-slim`

	poi:

	`sudo systemctl enable slim.service`

	editare:

	`sudo nano /etc/slim.conf`

	cambiare la riga:

	```
	current_theme default
	```

	con:

	```
	current_theme archlinux-soft-grey
	```

11) <a name="post-installare-pamac"></a> **Installare PAMAC (gestore grafico dei pacchetti)**

	**NOTA BENE**: installare PAMAC-AUR o PAMAC-ALL se non si è installato il sistema da Calam installer.

	**PAMAC-AUR (con supporto AUR)**

	`mkdir ~/temp` (se non già creata precedentemente)

	`cd ~/temp`

	`git clone https://aur.archlinux.org/pamac-aur.git`

	`makepkg -sic BUILDDIR=/home/giagio/temp/pamac-aur`

	**se si vuole rimuovere la cartella di pamac-aur (opzionale)**:
	
	`rm -fr ~/temp/pamac-aur`
	
	**PAMAC-ALL (oltre AUR include anche snap, flatpak, appindicator)**

	`mkdir ~/temp` (se non già creata precedentemente)

	`cd ~/temp`

	`git clone https://aur.archlinux.org/pamac-all.git`

	`makepkg -sic BUILDDIR=/home/giagio/temp/pamac-all`

	**se si vuole rimuovere la cartella di pamac-all (opzionale)**:
	
	`rm -fr ~/temp/pamac-all`

12) <a name="post-installare-pacchetti-utili"></a> **Installare pacchetti utili**

	**bash-completion**, **gvfs**, **gvfs-smb**, **htop**, **wget**, **xdg-user-dirs** (crea cartelle utente come ~/Music, ~/Documents, etc), **gnome-keyring**, **seahorse**

	`sudo pacman -S bash-completion gvfs gvfs-smb htop wget xdg-user-dirs gnome-keyring seahorse`

13) <a name="post-stile-qt5"></a> **Uniformare lo stile delle applicazioni QT5 con stile GTK+**

	Vedi anche Wiki ufficiale: **https://wiki.archlinux.org/index.php/Uniform_look_for_Qt_and_GTK_applications#QGtkStyle**

	Installare il pacchetto **qt5-styleplugins** dal repository **AUR**:

	`paru qt5-styleplugins`

	Per le applicazioni in versione **Qt4** creare il seguente file:

	`nano ~/.config/Trolltech.conf`

	ed aggiungere le seguenti righe:

	```
	[Qt]
	style=GTK+
	```

	mentre per le applicazioni in versione **Qt5** editare il file:

	`sudo nano /etc/environment`

	ed aggiungere la riga:

	```
	QT_QPA_PLATFORMTHEME=gtk2
	```
	
	**SOLUZIONE FUNZIONANTE SE SI UTILIZZA ANCHE VIRTUALBOX**
	
	Se si utilizza **Virtualbox** (applicazione scritta in Qt5), le soluzioni sopra non uniformano l'aspetto della GUI di Virtualbox al tema GTK che si utilizza; ho trovato una soluzione più elegante che funziona, oltre che con Virtualbox, anche con tutte le altre applicazioni scritte in Qt (vedi VLC, Owncloud Client, eccetera).
	
	Sarà necessario installare il pacchetto **kvantum-qt5**:
	
	`sudo pacman -S kvantum-qt5`
	
	inoltre installare, se disponibile, il tema per Kvantum corrispondente al tema GTK che si utilizza, esempio per il tema Qogir scaricare il repository dal link:
	
	**https://github.com/vinceliuice/Qogir-kde**
	
	e poi copiare le sottocartelle della cartella Kvantum in **~/.config/Kvantum**. A questo punto selezionare il tema nel Kvantum Manager, poi editare il file:
	
	`sudo nano /etc/environment`
	
	ed aggiungere la riga:

	```
	QT_STYLE_OVERRIDE=kvantum
	```
	questa soluzione è testata e funzionante al 100%

14) <a name="post-installare-samba"></a> **Installare SAMBA**

	`sudo pacman -S samba`

	`sudo systemctl enable smb.service`

	scaricare il file smb.conf.default da:

	**https://git.samba.org/samba.git/?p=samba.git;a=blob_plain;f=examples/smb.conf.default;hb=HEAD**

	copiare il file in /etc/samba/smb.conf

	`sudo cp /etc/samba/smb.conf`

	controllare il file smb.conf con:

	`testparm`

	creare un gruppo (deve essere fatto come root):

	`export USERSHARES_DIR="/var/lib/samba/usershares"`

	`export USERSHARES_GROUP="sambashare"`

	`mkdir -p ${USERSHARES_DIR}`

	`groupadd ${USERSHARES_GROUP}`

	`chown root:${USERSHARES_GROUP} ${USERSHARES_DIR}`

	`chmod 01770 ${USERSHARES_DIR}`

	modificare il file **/etc/samba/smb.conf**:

	nella sezione [global] modificare:

	```
	workgroup = WORKGROUP
	```

	modificare:

	```
	log file = /var/log/samba/log.%m
	```

	aggiungere:

	```
	usershare path = /var/lib/samba/usershares
	usershare max shares = 100
	usershare allow guests = yes
	usershare owner only = yes
	client min protocol = CORE (per condivisioni samba al di sotto della versione 3)
	```

	aggiungere il gruppo samba al tuo utente (sostituisci tuo_nomeutente per il tuo login):

	`usermod -a -G ${USERSHARES_GROUP} tuo_nomeutente`

	Avvia Samba:

	`sudo systemctl start smb.service`

	inoltre:

	`sudo smbpasswd -a tuo_nomeutente`

	Logout e login del tuo utente.

<a name="terza-parte"></a>
## Terza parte - Installazione di un desktop grafico

<a name="desktop-cinnamon"></a>
### Installazione del desktop grafico Cinnamon

Per il desktop grafico Cinnamon ( **solo se non si è installato il sistema da Calam Installer** ) installare i pacchetti come da tabella:

| pacchetto                | note                                                       |
| :----------------------: | ---------------------------------------------------------- |
| **bluez**                | demone per il bluetooth                                    |
| **bluez-utils**          | -                                                          |
| **blueman**              | manager per i dispositivi bluetooth                        |
| **cinnamon**             | -                                                          |
| **gnome-terminal**       | -                                                          |
| **gnome-keyring**        | -                                                          |
| **gnome-logs**           | -                                                          |
| **gnome-system-log**     | -                                                          |
| **gnome-system-monitor** | -                                                          |
| **gvfs**                 | -                                                          |
| **gvfs-smb**             | -                                                          |
| **nemo-fileroller**      | estensione per file manager nemo                           |
| **nemo-image-converter** | estensione per file manager nemo                           |
| **nemo-preview**         | estensione per file manager nemo                           |
| **nemo-python**          | estensione per file manager nemo                           |
| **nemo-seahorse**        | estensione per file manager nemo                           |
| **nemo-share**           | estensione per file manager nemo                           |
| **nemo-terminal**        | estensione per file manager nemo                           |
| **pantheon-calendar**    | al posto di gnome-calendar, che installa troppe dipendenze |
| **pulseaudio**           | -                                                          |
| **pulseaudio-equalizer** | eseguibile: /usr/bin/qpaeq                                 |
| **pulseaudio-bluetooth** | -                                                          |
| **pavucontrol**          | -                                                          |

`sudo pacman -S bluez bluez-utils blueman cinnamon gnome-terminal gnome-keyring gnome-logs gnome-system-log gnome-system-monitor gvfs gvfs-smb nemo-fileroller nemo-image-converter nemo-preview nemo-python nemo-seahorse nemo-share nemo-terminal pantheon-calendar pulseaudio pulseaudio-equalizer pulseaudio-bluetooth pavucontrol`

e alcune librerie per risolvere alcuni errori in .xsession-errors:

`sudo pacman -S ruby lua aspell nuspell hspell libvoikko`

abilitare il servizio bluetooth:

`sudo systemctl enable bluetooth`

Dal repository **AUR** installare i seguenti pacchetti:

`paru cinnamon-sound-effects`

`paru folder-color-nemo`

`paru nemo-audio-tab`

`paru nemo-emblems`

`paru nemo-compare`

`paru nemo-media-columns`

riavviare il sistema con:

`sudo reboot`

**Risolvere errore cinnamon-settings-daemon-smartcard**

Se nel file ~/.xsession-errors viene mostrato l'errore:

```
[cinnamon-settings-daemon-smartcard] Failed to start: no suitable smartcard driver could be found
```

basta aggiungere (o sostituire quelle esistenti) le seguenti righe al file **/etc/xdg/autostart/cinnamon-settings-daemon-smartcard.desktop**:

```
X-GNOME-Autostart-enabled=false
X-GNOME-Autostart-Phase=false
X-GNOME-Autostart-Notify=false
X-GNOME-AutoRestart=false
```

---

<a name="desktop-xfce"></a>
### Installazione del desktop grafico Xfce

| pacchetto                    | note                                                       |
| :--------------------------: | ---------------------------------------------------------- |
| **xfce4**                    | -                                                          |
| **xfce4-goodies**            | -                                                          |
| **gvfs**                     | -                                                          |
| **gvfs-smb**                 | -                                                          |
| **pulseaudio**               | -                                                          |
| **pulseaudio-equalizer**     | eseguibile: /usr/bin/qpaeq                                 |
| **pulseaudio-bluetooth**     | -                                                          |
| **pavucontrol**              | -                                                          |
| **thunar-archive-plugin**    | -                                                          |
| **thunar-media-tags-plugin** | -                                                          |
| **thunar-volman**            | -                                                          |
| **bluez**                    | pacchetto per bluetooth                                    |
| **bluez-utils**              | pacchetto per bluetooth                                    |
| **blueman**                  | pacchetto per bluetooth                                    |

`sudo pacman -S xfce4 xfce4-goodies gvfs gvfs-smb pulseaudio pulseaudio-equalizer pulseaudio-bluetooth pavucontrol thunar-archive-plugin thunar-media-tags-plugin thunar-volman bluez bluez-utils blueman`

abilitare il servizio bluetooth:

`sudo systemctl enable bluetooth`

mugshot (AUR):

`paru mugshot`

menulibre (AUR):

`paru menulibre`

thunar-shares-plugin (AUR):

`paru thunar-shares-plugin`

**NOTA BENE**: per far funzionare il plugin, vedi anche **Parte 2 - installazione di Samba punto 13)** (configurazione usershares).

xfce4-panel-profiles (AUR):

`paru xfce4-panel-profiles`

xfce4-kbdleds-plugin (AUR):

`paru xfce4-kbdleds-plugin`

**NOTA BENE**: verificare bene se il plugin funziona correttamente.

xfce4-dockbarx-plugin (AUR):

`paru dockbarx`
`paru xfce4-dockbarx-plugin`

arc-dark-xfce4-terminal (AUR):

`paru arc-dark-xfce4-terminal`

Comandi utili per settare layout keyboard

`setxkbmap -print -verbose 10`

`sudo nano /etc/X11/xinit/xinitrc`

`setxkbmap it`

`localectl --no-convert set-x11-keymap it pc105`

`cat /etc/X11/xorg.conf.d/00-keyboard.conf`

riavviare il sistema con:

`sudo reboot`

<a name="quarta-parte"></a>
## Quarta parte - Installazione di software vario

<a name="stampa"></a>
### Stampa e drivers stampanti

Installare i seguenti pacchetti base:

**system-config-printer**, **cups**, **cups-pdf**

`sudo pacman -S system-config-printer cups cups-pdf`

abilitare il servizio cups:

`sudo systemctl enable cups.service`

installazione del driver per stampante Epson Workforce WF-2010w dal repo **AUR**:

`paru epson-inkjet-printer-201211w`

---

<a name="fonts-aggiuntivi"></a>
### Fonts aggiuntivi

Installare i seguenti pacchetti di fonts aggiuntivi:

**noto-fonts**, **noto-fonts-extra**, **powerline-fonts**, **ttf-anonymous-pro**, **ttf-bitstream-vera**, **ttf-droid**, **ttf-eurof**, **ttf-caladea**, **ttf-carlito**, **ttf-dejavu**, **ttf-fantasque-sans-mono**, **ttf-fira-code**, **ttf-fira-mono**, **ttf-fira-sans**, **ttf-font-awesome**, **ttf-hack**, **ttf-lato**, **ttf-liberation**, **libertinus-font**, **ttf-linux-libertine-g**, **ttf-nerd-fonts-symbols**, **ttf-opensans**, **ttf-roboto**, **ttf-roboto-mono**, **ttf-ubuntu-font-family**

**Se hai installato il sistema da iso di Arch Linux**:

`sudo pacman -S noto-fonts noto-fonts-extra powerline-fonts ttf-anonymous-pro ttf-bitstream-vera ttf-caladea ttf-carlito ttf-dejavu ttf-droid ttf-fantasque-sans-mono ttf-hack ttf-fira-code ttf-fira-mono ttf-fira-sans ttf-font-awesome ttf-lato ttf-liberation libertinus-font ttf-linux-libertine-g ttf-nerd-fonts-symbols ttf-opensans ttf-roboto ttf-roboto-mono ttf-ubuntu-font-family`

**Se hai installato il sistema da iso di Calam Installer**:

`sudo pacman -S noto-fonts-extra powerline-fonts ttf-anonymous-pro ttf-droid ttf-fantasque-sans-mono ttf-hack ttf-fira-code ttf-fira-mono ttf-fira-sans ttf-font-awesome ttf-lato libertinus-font ttf-linux-libertine-g ttf-nerd-fonts-symbols ttf-roboto ttf-roboto-mono`

inoltre:

`sudo pacman -S gnome-font-viewer`

`paru ttf-symbola`

`paru ttf-ms-fonts`

`paru nerd-fonts-source-code-pro`

`paru font-manager`

---

<a name="gestori-archivi"></a>
### Gestori archivi

Installare i seguenti pacchetti per la gestione degli archivi compressi:

**arj**, **lrzip**, **p7zip**, **rsync**, **sharutils**, **squashfs-tools**, **tar**, **uudeview**, **unrar**, **unzip**, **unace**, **xarchiver** (o, alternativamente, **file-roller**)

**Se hai installato il sistema da iso di Arch Linux**:

`sudo pacman -S arj lrzip p7zip rsync sharutils squashfs-tools tar uudeview unrar unzip unace`

**Se hai installato il sistema da iso di Calam Installer**:

`sudo pacman -S arj lrzip p7zip sharutils uudeview unace`

poi:

`sudo pacman -S xarchiver`

oppure:

`sudo pacman -S file-roller`

---

<a name="browser-mail"></a>
### Browser Web e Client Email

Per un browser web, dal repository principale scegliere tra:

**firefox**, **seamonkey**, **chromium**, **opera**, **midori**, **vivaldi**

Per il client di posta elettronica, dal repository principale scegliere:

**thunderbird**

`sudo pacman -S thunderbird vivaldi`

**Google Chrome**:

`paru google-chrome`

**Brave Browser**:

`paru brave-bin`

**Dissenter Browser**:

`paru dissenter-browser-bin`

**Flash plugin per Chromium**:

`paru flashplugin`

---

<a name="codec-multimedia"></a>
### Codec e utility multimediali

**Se hai installato il sistema da iso di Arch Linux**:

`sudo pacman -S a52dec acetoneiso2 asunder audacity brasero cdparanoia cdrdao cdrtools devede dvd+rw-tools dvdauthor dvdbackup dvgrab easytag faac faad2 flac ffmpegthumbs frei0r-plugins gst-plugins-base gst-plugins-good gst-plugins-ugly gst-libav gstreamer gthumb guvcview handbrake jasper kid3-qt kodi kodi-x11 lame libdca libdv libdvdcss libdvdnav libdvdread libmad libmpeg2 libtheora libvorbis libxv obs-studio rhythmbox screenkey shotwell simplescreenrecorder soundconverter twolame vlc vorbis-tools wavpack x264 xvidcore youtube-dl`

**Se hai installato il sistema da iso di Calam Installer**:

`sudo pacman -S acetoneiso2 asunder audacity brasero cdrdao devede dvd+rw-tools dvdauthor dvdbackup dvgrab easytag ffmpegthumbs frei0r-plugins gthumb guvcview handbrake jasper kid3-qt kodi kodi-x11 obs-studio rhythmbox screenkey shotwell simplescreenrecorder soundconverter vlc vorbis-tools youtube-dl`

Dal repository **AUR** installa i seguenti pacchetti aggiuntivi:

`paru dvdisaster`

`paru puddletag`

`paru rhythmbox-plugin-alternative-toolbar`

`paru winff`

`paru youtube-dl-gui-git`

---

<a name="java"></a>
### Oracle JAVA e OpenJDK

`sudo pacman -S jre-openjdk jre-openjdk-headless`

---

<a name="temi-gtk"></a>
### Temi GTK (opzionali)

Dal repository principale:

`sudo pacman -S adapta-gtk-theme arc-solid-gtk-theme materia-gtk-theme arc-gtk-theme arc-icon-theme`

solarized-dark-themes ( da repository **AUR** ):

`paru solarized-dark-themes`

---

<a name="temi-icone"></a>
### Temi icone (opzionali)

`sudo pacman -S arc-icon-theme papirus-icon-theme`

papirus-folders ( da repository **AUR** ):

`paru papirus-folders`

papirus-folders-nordic ( da repository **AUR** ):

`paru papirus-folders-nordic`

papirus-filezilla-themes ( da repository **AUR** ):

`paru papirus-filezilla-themes`

papirus-libreoffice-theme ( da repository **AUR** ):

`paru papirus-libreoffice-theme`

---

<a name="wallpapers"></a>
### Wallpapers (opzionali)

Dal repository principale:

`sudo pacman -S archlinux-wallpaper`

Gestore di wallpapers **variety**:

`sudo pacman -S variety`

Wallpapers dal repository Gitlab di DistroTube:

`git clone https://gitlab.com/dwt1/wallpapers.git`

---

<a name="grafica"></a>
### Programmi di grafica (opzionali)

`sudo pacman -S dia gpick inkscape mypaint mypaint-brushes1 peek pinta rapid-photo-downloader`

**NOTA BENE**: non installare Gimp dai repository principali, poiché il pacchetto dei plugins di gimp **gimp-plugin-registry** da un errore di compilazione. Al suo posto utilizzare la versione in formato AppImage:

**https://github.com/aferrero2707/gimp-appimage/releases/download/continuous/GIMP_AppImage-release-2.10.22-withplugins-x86_64.AppImage**

oppure si può installare la versione flatpak.

---

<a name="virtualbox"></a>
### Virtualbox

Vedi anche Wiki ufficiale: **https://wiki.archlinux.org/index.php/VirtualBox**

`sudo pacman -S virtualbox virtualbox-guest-iso`

per abilitare i dispositivi usb a livello di utente:

`sudo usermod -aG vboxusers <nome_utente>`

successivamente, scaricare il **VirtualBox Extension Pack** dal seguente indirizzo:

**https://download.virtualbox.org/virtualbox/6.1.32/Oracle_VM_VirtualBox_Extension_Pack-6.1.32.vbox-extpack** (aggiornato all'ultima versione)

Pagina ufficiale dei downloads di Virtualbox: **https://www.virtualbox.org/wiki/Downloads**

Per correggere l'impossibilità di accedere ad una cartella condivisa, poiché non ci sono i permessi per accedervi:'

`sudo usermod -a -G vboxsf <nome_utente>`

---

<a name="virt-manager"></a>
### Virt-Manager, Qemu e Libvirtd (alternativa a Virtualbox)

`sudo pacman -S qemu virt-manager ebtables`

`sudo systemctl enable libvirtd`

`sudo systemctl start libvirtd`

`sudo usermod -aG libvirtd <nome_utente>`

per convertire un disco virtualbox a qemu:

`sudo qemu-img convert -f vdi -O qcow2 <disco-immagine.vdi> /var/lib/libvirt/images/<disco-immagine.qcow2>`

---

<a name="gestori-downloads"></a>
### Gestori downloads

`sudo pacman -S aria2 deluge-gtk filezilla uget`

**NOTA BENE**: se ti colleghi con Filezilla ad un server pure-ftpd che non è aggiornato alla versione 1.0.47 o successive, **non installare** filezilla, perchè con le versioni superiori alla 3.28 non funziona l’autenticazione TLS. Scarica la versione 3.28 dal seguente link:

**https://download.filezilla-project.org/client/FileZilla_3.28.0_x86_64-linux-gnu.tar.bz2**

per il problema con il server pure-ftpd vedi anche: 

**https://portal.databasemart.com/kb/a528/pure-ftp-gnutls-error-110-in-gnutls-record-recv-the-tls-connection-was-non-properly-terminated.aspx**

---

<a name="skype"></a>
### Skype (opzionale)

`paru skypeforlinux-stable-bin`

---

<a name="libreoffice"></a>
### Libreoffice

`sudo pacman -S libreoffice-fresh` (ultima versione disponibile, non stabile)

`sudo pacman -S libreoffice-still` (versione stabile)

---

<a name="owncloud-client"></a>
### Owncloud Client

`sudo pacman -S owncloud-client`

per disabilitare la notifica dell'update:

`nano $HOME/.config/ownCloud/owncloud.cfg`

settare:

```
skipUpdateCheck = true
```

---

<a name="code"></a>
### Visual Studio Code

`sudo pacman -S code`

Vedi anche: https://code.visualstudio.com/

---

<a name="clients-chat"></a>
### Clients chat

`sudo pacman -S telegram-desktop hexchat`

---

<a name="wine"></a>
### Wine (opzionale)

`sudo pacman -S wine playonlinux`

---

<a name="bleachbit"></a>
### Bleachbit (opzionale)

`sudo pacman -S bleachbit`

---

<a name="remmina"></a>
### Remmina (opzionale)

applicazione con icona systray funzionante:

`paru remmina-appindicator`

remmina freeRDP (Remote Desktop Protocol):

`sudo pacman -S freerdp`

remmina-plugin-rdesktop (AUR):

`paru remmina-plugin-rdesktop`

---

<a name="ocs-url"></a>
### ocs-url (AUR)

**ocs-url** è un programma di supporto per l'installazione degli elementi serviti tramite OpenCollaborationServices (ocs: //). Nello specifico, quando ad esempio si va nel sito **https://www.gnome-look.org/**, per ogni tema, set di icone, eccetera c'è un pulsante Install, se è installato ocs-url viene un tema, set icone installato automaticamente nel sistema.

`paru ocs-url`

---

<a name="key-mon"></a>
### key-mon (AUR)

**key-mon** è una piccola utility per visualizzare lo stato corrente della tastiera e del mouse. Utile per screencast.

`paru key-mon`

---

<a name="ulauncher"></a>
### ulauncher (AUR)

**ulauncher** è un programma per eseguire applicazioni di Linux tramite un comodo menu a scomparsa, che viene visualizzato tramite una scorciatoia da tastiera pre-impostata.

`paru ulauncher`

Vedi anche: **https://github.com/Ulauncher/Ulauncher**

Sito ufficiale: **https://ulauncher.io/**

---

<a name="update-grub"></a>
### update-grub (AUR)

**update-grub** è un semplice wrapper per eseguire grub-mkconfig.

`paru update-grub`

---

<a name="hblock"></a>
### hblock (AUR)

**hblock** è un Adblocker che crea un file host da più origini.

`paru hblock`

vedi anche: **https://github.com/hectorm/hblock**

---

<a name="hardcode-tray"></a>
### hardcode-tray-git (AUR)

**hardcode-tray** corregge le icone del pannello.

`paru hardcode-tray-git`

vedi anche: **https://github.com/bilelmoussaoui/Hardcode-Tray**

---

<a name="timeshift"></a>
### timeshift (AUR)

**timeshift** è una utilità di ripristino del sistema per Linux.

`paru timeshift`

vedi anche: **https://github.com/teejee2008/timeshift**

---

<a name="teamviewer"></a>
### teamviewer (AUR)

**teamviewer** è un software all-in-one per supporto remoto e riunioni online.

`paru teamviewer`

Per abilitare e avviare il servizio di teamviewer:

`sudo systemctl enable teamviewerd.service`

`sudo systemctl start teamviewerd.service`

---

<a name="slurm"></a>
### slurm (AUR)

**slurm** è un programma per il monitoraggio delle statistiche sul traffico della rete in tempo reale.

`paru slurm`

---

<a name="pfetch"></a>
### pfetch (AUR)

**pfetch** è un grazioso strumento per le informazioni di sistema scritto in POSIX sh.

`paru pfetch`

---

<a name="qownnotes"></a>
### qownnotes (AUR)

**qownnotes** è un programma per appunti su file di testo con integrazione Nextcloud / ownCloud. Questo pacchetto è la versione "ufficiale" di QOwnNotes su AUR.

`paru qownnotes`

vedi anche: **https://www.qownnotes.org/**

---

<a name="clipit"></a>
### clipit (AUR)

**clipit** è un leggero gestore di appunti scritto in GTK+ (fork di Parcellite)

paru clipit

---

<a name="toilet"></a>
### toilet (AUR)

**toilet** è una sostituzione gratuita dell'utilità FIGlet.

`paru toilet`

---

<a name="conky-lua"></a>
### conky-lua (AUR)

**conky-lua** è un monitor di sistema leggero per X, con supporto Lua abilitato.

`paru conky-lua`

---

<a name="gnome-system-tools"></a>
### gnome-system-tools (AUR)

**gnome-system-tools** è una utilità di configurazione multipiattaforma per GNOME.

prima di installare gnome-system-tools, è necessario installare altri due pacchetti, system-tools-backends e liboobs (libreria per gnome-system-tools):

`paru system-tools-backends`

poi:

`cd ~/temp` (se non esiste, creare la cartella temp)

`git clone https://aur.archlinux.org/liboobs.git`

`cd liboobs`

edita il file PKGBUILD, cambia “ftp://ftp.gnome.org…” in “https://ftp.gnome.org…”

`makepkg -si`

infine:

`paru gnome-system-tools`

---

<a name="utility-varie"></a>
### Utility varie

**Se hai installato il sistema da iso di Arch Linux**:

`sudo pacman -S accountsservice baobab bashtop bat bpytop catfish dconf-editor dstat etherape exfat-utils figlet flameshot geany geany-plugins glances gnome-disk-utility gnome-mahjongg gnote gparted gprename grsync gtk-engine-murrine gufw hardinfo hwinfo iotop ipcalc iptraf-ng jq keepassxc librecad liferea lolcat mc meld micro multitail ncdu neofetch nmap nmon openssh plank powertop psensor putty pv qalculate-gtk rmlint s-tui smartmontools stress sysbench tilda tilix tree vifm vnstat xclip xed`

**Se hai installato il sistema da iso di Calam Installer**:

`sudo pacman -S baobab bashtop bat bpytop catfish dconf-editor dstat etherape figlet flameshot geany geany-plugins gnome-disk-utility gnome-mahjongg gnote gprename grsync gtk-engine-murrine gufw iotop ipcalc iptraf-ng jq keepassxc librecad liferea lolcat mc meld micro multitail ncdu nmap nmon plank powertop psensor putty qalculate-gtk rmlint s-tui stress sysbench tilda tilix tree vifm vnstat xclip`

---

<a name="altri-pacchetti-utili"></a>
### Altri pacchetti utili

| Pacchetto                          | Note                                                                                       |
| :--------------------------------: | ------------------------------------------------------------------------------------------ |
| **acpi**                           | Client for battery, power, and thermal readings                                            |
| **acpid**                          | A daemon for delivering ACPI power management events with netlink support                  |
| **apr**                            | The Apache Portable Runtime                                                                |
| **apr-util**                       | The Apache Portable Runtime                                                                |
| **ca-certificates**                | Common CA certificates (default providers)                                                 |
| **calibre**                        | Ebook management application                                                               |
| **cmatrix**                        | A curses-based scrolling 'Matrix'-like screen                                              |
| **cpupower**                       | Linux kernel tool to examine and tune power saving related features of your processor      |
| **doublecmd-gtk2**                 | twin-panel (commander-style) file manager (GTK2)                                           |
| **doublecmd-qt5**                  | twin-panel (commander-style) file manager (Qt5)                                            |
| **dmidecode**                      | Desktop Management Interface table related utilities                                       |
| **downgrade (AUR)**                | Bash script for downgrading one or more packages to a version in your cache or the A.L.A.  |
| **gsmartcontrol**                  | A graphical user interface for the smartctl hard disk drive health inspection tool         |
| **gtkhash (AUR)**                  | A GTK+ utility for computing message digests or checksums                                  |
| **gtkhash-nemo (AUR)**             | A GTK+ utility for computing message digests or checksums (Nemo filemanager plugin)        |
| **httrack**                        | An easy-to-use offline browser utility                                                     |
| **inxi (AUR)**                     | Full featured CLI system information tool                                                  |
| **lightdm-settings (AUR)**         | A configuration tool for the LightDM display manager                                       |
| **lollypop**                       | Music player for GNOME                                                                     |
| **memtest86+**                     | An advanced memory diagnostic tool                                                         |
| **mousetweaks**                    | Mouse accessibility enhancements                                                           |
| **msmtp**                          | A mini smtp client                                                                         |
| **msmtp-mta**                      | A mini smtp client - the regular MTA                                                       |
| **ntp**                            | Network Time Protocol reference implementation                                             |
| **parcellite**                     | Lightweight GTK+ clipboard manager                                                         |
| **partclone**                      | Utilities to save and restore used blocks on a partition                                   |
| **playonlinux**                    | GUI for managing Windows programs under linux                                              |
| **pulseaudio-ctl (AUR)**           | Control pulseaudio volume from the shell or mapped to keyboard shortcuts                   |
| **spectre-meltdown-checker (AUR)** | Spectre, Meltdown, Foreshadow, Fallout, RIDL, ZombieLoad vulnerability/mitigation checker  |
| **terminator**                     | Terminal emulator that supports tabs and grids                                             |
| **terminus-font**                  | Monospace bitmap font (for X11 and console)                                                |
| **tpl**                            | Linux Advanced Power Management                                                            |
| **traceroute**                     | Tracks the route taken by packets over an IP network                                       |
| **yad**                            | A fork of zenity - display graphical dialogs from shell scripts or command line            |

<a name="quinta-parte"></a>
## Quinta parte - Alcune ottimizzazioni di sistema

<a name="chiavi-autenticazione"></a>
### Aggiornare le chiavi di autenticazione

`sudo pacman -Sy archlinux-keyring`

`sudo pacman-key --refresh-keys`

---

<a name="crontab"></a>
### Crontab (cronie)

`sudo pacman -S cronie`

---

<a name="pulizia-cache-pacchetti"></a>
### Abilitare il servizio per la pulizia cache pacchetti

`sudo systemctl enable paccache.timer`

`sudo systemctl start paccache.timer`

`sudo systemctl status paccache.timer`

---

<a name="ssd-trim"></a>
### Solid State Drive (SSD trim)

Vedi: **https://wiki.archlinux.org/index.php/Solid_state_drive#Periodic_TRIM**

Trim periodico (consigliato):

`sudo systemctl enable fstrim.timer`

`sudo systemctl start fstrim.timer`

`sudo systemctl status fstrim.timer`

poi, dal file **/etc/fstab** per i dischi SSD, dove trovi **discard** cancellare.

---

<a name="hw-audio"></a>
### Problemi relativi all'hardware audio

Vedi: **https://wiki.archlinux.org/index.php/Advanced_Linux_Sound_Architecture/Troubleshooting#Pops_when_starting_and_stopping_playback**

**Strani pop quando si avvia e si arresta la riproduzione audio**

Alcuni moduli (ad esempio **snd_ac97_codec** e **snd_hda_intel**) possono spegnere la scheda audio quando non viene utilizzata. Questo può produrre un rumore udibile (come uno schiocco / pop / graffio) quando si accende e si spegne la scheda audio. A volte anche quando si sposta il cursore del volume o si aprono e si chiudono le finestre (KDE4). Se trovi questo fastidioso, prova **modinfo your_module** e cerca un'opzione del modulo che regola o disabilita questa funzione.

Ad esempio, per disabilitare la modalità di risparmio energetico per il modulo **snd_hda_intel**, aggiungere a **/etc/modprobe.d/modprobe.conf**:

```
options snd_hda_intel power_save=0
```

Potrebbe anche essere necessario disabilitare il risparmio energetico per il controller della scheda audio:

```
options snd_hda_intel power_save=0 power_save_controller=N
```

In alternativa puoi impostare i parametri per i moduli del kernel con:

`modprobe snd_hda_intel power_save=0`

Potrebbe anche essere necessario riattivare il canale ALSA "Line" affinché funzioni. Qualsiasi valore andrà bene (diverso da "0" o qualcosa di troppo alto). Ad esempio, su un VIA VT1708S integrato (utilizzando il modulo snd_hda_intel) queste crepe si sono verificate anche quando power_save era impostato su 0. L'attivazione del canale "Line" e l'impostazione del valore "1" hanno risolto il problema.

<a name="sesta-parte"></a>
## Sesta parte - Risorse varie dal web

<a name="risorsa1"></a>
### Arch Linux: Full Installation Guide - A complete tutorial/walkthrough in one video!

Video Youtube: **https://www.youtube.com/watch?v=DPLnBPM4DhI**

qui puoi trovare il Wiki con tutti i comandi mostrati nel video:

**https://wiki.learnlinux.tv/index.php/Arch_Linux_-_Full_installation_Guide**

---

<a name="risorsa2"></a>
### Connect Android To Arch Linux Via USB – Connect over MTP in Linux

Questo articolo descrive come connettere Android ad Arch Linux tramite USB e MTP.

Qui trovi l'articolo completo in inglese:

**https://www.linuxfordevices.com/tutorials/linux/connect-android-to-arch-via-usb**

Di seguito, sono riportati i principali comandi per abilitare MTP e PTP.

**Abilitare il supporto MTP con mtpfs**

Innanzitutto, per abilitare il supporto MTP ( **Media Transfer Protocol** ) dobbiamo installare il seguente pacchetto:

`sudo pacman -S mtpfs`

Per dispositivi con Android 4+, dovrebbe essere sufficiente. Tuttavia, nelle versioni successive, avremmo bisogno di un altro pacchetto chiamato **jmtpfs** dal repository AUR. Puoi installarlo tramite il seguente comando:

`paru jmtpfs`

A questo punto, abbiamo MTP abilitato. Tuttavia, non sarebbe ancora visibile nel tuo File Manager in quanto non è montato automaticamente come desideriamo. Per montarlo automaticamente, dobbiamo installare un pacchetto con il comando:

`sudo pacman -S gvfs-mtp`

**Abilitare il supporto PTP**

PTP sta per **Picture Transfer Protocol** e infatti è il protocollo su cui si basa MTP. Quando comunichi con il tuo telefono Android tramite PTP, appare come una fotocamera digitale sul tuo PC.

Per abilitare PTP, dobbiamo installare un pacchetto con il comando:

`sudo pacman -Sy gvfs-gphoto2`

Infine, affinché le modifiche abbiano effetto, è necessario riavviare il sistema che può essere eseguito con:

`reboot`

---

<a name="risorsa3"></a>
### How to clean Arch Linux

Vedi articolo: **https://averagelinuxuser.com/clean-arch-linux/#1-clean-package-cache**

---

<a name="risorsa4"></a>
### ArcoLinux Listing - Elenco di utili risorse valide anche per Arch Linux

Vedi pagina: **https://arcolinux.com/listing/**

---

<a name="risorsa5"></a>
### How To Speed Up Compilation Process When Installing Packages From AUR

Vedi pagina: **https://ostechnix.com/speed-compilation-process-installing-packages-aur/**

---

<a name="risorsa6"></a>
### Enable High Quality Audio on Linux

Vedi pagina: **https://medium.com/@gamunu/enable-high-quality-audio-on-linux-6f16f3fe7e1f**

Per ottimizzare l'audio, editare il file **~/.config/pulse/daemon.conf** e aggiungere le seguenti righe:

```
default-sample-format = float32le
default-sample-rate = 48000
alternate-sample-rate = 44100
default-sample-channels = 2
default-channel-map = front-left,front-right
default-fragments = 2
default-fragment-size-msec = 125
# resample-method = speex-float-10
resample-method = soxr-vhq
# enable-lfe-remixing = no
remixing-produce-lfe = no
remixing-consume-lfe = no
high-priority = yes
nice-level = -11
realtime-scheduling = yes
realtime-priority = 9
rlimit-rtprio = 9
daemonize = no
avoid-resampling = true
```

Riavviare per rendere effettive le modifiche.

---

<a name="risorsa7"></a>
### Different Types of Kernel for Arch Linux and How to Use Them

Vedi pagina: **https://itsfoss.com/switch-kernels-arch-linux/**

---

<a name="risorsa8"></a>
### vinceliuice on Github - grub2 themes

Vedi pagina: **https://github.com/vinceliuice/grub2-themes**

---

<a name="risorsa9"></a>
### Glava - An embed audio visualizer on your linux

Vedi pagina: **https://www.linuxuprising.com/2018/11/embed-audio-visualizer-on-your-linux.html**

---

<a name="risorsa10"></a>
### MacOS like Fonts on Manjaro/Arch Linux

Vedi pagina: **https://aswinmohan.me/posts/better-fonts-on-linux/**

---
